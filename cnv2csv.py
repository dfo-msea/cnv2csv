import csv
import os
import glob
import shutil
import sys

FILE_EXT = "cnv"
HEADER_SEARCH = "# name"
CTD_SEARCH = "  "
OUTPUT_EXT = ".csv"


def backup_file(in_file):
    """Copy IN_FILE to same directory with '.bak' extension.
    C:\Temp\coral2.cnv --> C:\Temp\coral2.cnv.bak

    in_file: path where cnv file is located.
    """
    out_file = in_file + ".bak"
    shutil.copy2(in_file, out_file)
    return out_file


def get_filepath(txt_file, file_extension):
    """Return the file with matching FILE_EXTENSION that is located in the directory 
    specified in TXT_FILE. 

    txt_file: input text file containing a single line that is a path. 
    file_extension: file extension used to search directory.
    """
    with open(txt_file, "r", encoding="UTF-8") as infile:
        in_dir = infile.readline()
        return glob.glob(os.path.join(in_dir, f"*.{file_extension}"))[0]


def get_lines(filepath):
    """Read all lines from text file as a list of lists.
    
    filepath: full path to text file that contains data to be read.
    """
    with open(filepath, "r", encoding="UTF-8") as infile:
        return infile.readlines()


def get_rows(in_list, search_text):
    """Subset a list of lists where each element (list) contains a string that starts with SEARCH_TEXT.
    
    in_list: nested list where each element is a list with a string of data.
    search_text: string that is used to filter lists containing strings of data separated by whitespace.
    """
    return [i for i in in_list if i.startswith(search_text)]


def get_headers(header_rows):
    """Return processed string for each element in HEADER_ROWS to create list of header strings.
    '# name 0 = timeS: Time, Elapsed [seconds]' --> 'timeS: Time, Elapsed [seconds]'
    
    header_rows: list of strings containing header information from CNV file.
    """
    return [row.split(": ")[1].split("\n")[0] for row in header_rows]


def string_to_list(in_string):
    """Split string by whitespace (variable number of spaces separate CTD data in cnv file) to create list of CTD data.
    '      0.000     -0.271    10.2568' --> ['0.000', '-0.271', '10.2568']

    in_string: string to split by whitespace.
    """
    return in_string.split()


def make_filename(in_path, out_ext):
    """Return filename using IN_PATH and OUT_EXT as the new extension.
    C:\Temp\coral2.cnv --> C:\Temp\coral2.csv
    
    in_path: full filepath to an existing file.
    out_ext: string representing output extension (including '.') to append to new filepath.
    """
    base_name = os.path.basename(in_path).split(".")[0]
    base_dir = os.path.dirname(in_path)
    return os.path.join(base_dir, base_name + out_ext)


def write_file(in_list, out_filepath):
    """Write IN_LIST data to disk as OUT_FILEPATH.
    [[<header-element>], [<ctd-data>]] --> C:\Temp\coral2.csv

    in_list: data (nested list of lists where each nested element is either the header information or ctd data) 
    to write as csv file to disk.
    out_filepath: full filepath for writing new file to disk.
    """
    with open(out_filepath, "w", encoding="UTF-8", newline="") as f:
        writer = csv.writer(f)
        writer.writerows(in_list)
    return out_filepath


def process(input_directory):
    """Call functions to process cnv file and create csv file.
    
    input_directory: directory that contains cnv file and where output files are written.
    """
    input_filepath = get_filepath(input_directory, FILE_EXT)
    backup_filepath = backup_file(input_filepath)
    input_lines = get_lines(input_filepath)
    header_rows = get_rows(input_lines, HEADER_SEARCH)
    headers = get_headers(header_rows)
    ctd_rows = get_rows(input_lines, CTD_SEARCH)
    ctd_rows = [string_to_list(row) for row in ctd_rows]
    ctd_rows.insert(0, headers)
    out_filename = make_filename(input_filepath, OUTPUT_EXT)
    return write_file(ctd_rows, out_filename)


def main():
    # System arguments: drag and drop text file with directory where .cnv file is.
    input_dir = sys.argv[1]
    # Run program.
    process(input_dir)


if __name__ == "__main__":
    main()
