# CNV to CSV data conversion program.

__Main author:__  Cole Fields   
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Notes](#notes)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Backup cnv file and create csv file with the CTD data, pulled from cnv.


## Summary
This program is designed to create a backup copy of a cnv file and to write a new csv with headers and CTD data pulled from the cnv file. The executable file (`cnv2csv.exe`) is located in the `dist` directory. To execute the program, delete the existing path in the file `ctd_path.txt` (also located in the `dist` directory) and add a new path where a cnv file is located. There should be only one cnv file in the specified directory. Save and close the text file with new path. Drag and drop `ctd_path.txt` onto `cnv2csv.exe`. This will start the program and an output csv file should be written to disk. The purpose of the csv was to perform some quality control and plotting of the variables in R to ensure that the data looked as expected. Plotting the data can also easily be done in SeaSave software with the cnv file.


## Status
In-development


## Contents
* `cnv2csv.py`: python script that is converted into an executable which accepts a single argument (a path to a directory where a cnv file is located).
* `setup.py`: file required for py2exe conversion.
* `dist` directory that contains a number of files created from py2exe conversion.
    - `cnv2csv.exe`: executable file to run on computer.
    - `ctd_path.txt`: text file that contains a single line of text the is a directory where a cnv file is located. Update this file with a new path as required.


## Methods
1. Get a filepath using the path in `ctd_path.txt` file and glob to search for a cnv file in the directory. 
2. Backup the cnv file with '.bak' extension in the same directory.
3. Read in all lines from cnv file as a list of lists.
4. Subset list to get the ones that contain header information. Process the header lists to split into a list of strings that are the headers used in the output csv file.
5.  Search the remainder of the nested list for lists that contain CTD data (space delimited rows). Note that the text search for CTD data expects any new lines to start with whitespace - at least two spaces (i.e. '  '). Split string of data into list where each element is a record of CTD data from cnv file. Process each of the subset lists in this manner. Each nested list should have the same number of elements as the header list. 
6. Insert header list as the first element in the containing list (i.e. `[[<header-list>], [<ctd-data0>], [<ctd-data1>]`).
7. Write the data (list of lists) to a new csv file in the same directory specified in `ctd_path.txt`.


## Requirements
__Software__
* Does not require Python to be installed on the local machine. Requires the 'dist' folder and all the files within. cnv2csv.exe and a text file with a path are required to run the program successfully. 

__Source Data__
* .cnv file generated from Data Conversion module in SBEDataProcessing software. The SeaBird software converts raw hex data into human-readable text.


## Caveats
* No error checks in place.
* No logging to display if or where program fails. 


## Uncertainty
* If uncertain about the number of rows in the csv file, check the cvn which should show the start and end number of records that were processed from the CTD.


## Notes
This is a bare-bones program that was built quickly to automate the production of a csv file from the cnv data. The purpose of the csv was to perform some quality control and plotting of the variables in R to ensure that the data looked as expected. Plotting the data can also easily be done in SeaSave software with the cnv file.

## Acknowledgements


## References
